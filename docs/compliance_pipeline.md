# 🛠️ Compliance pipeline

!!! info
    - Documentation: [Compliant workflow automation](https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation)
    - Documentation: [Compliance pipelines](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#configure-a-compliance-pipeline) allow to define a pipeline configuration to run for any projects with a given [compliance framework](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html).


## 1- Define a compliance pipeline

- Import the project: `templates/compliance-pipeline` to create a new project and name it **"sast-compliance-pipeline"**.

This project contains the `.compliance-ci.yml` file:

```yaml
stages:
  - test
  - greetings

good_morning:
  stage: greetings
  tags: [ saas-linux-large-amd64 ]
  script: 
    - echo "🥐 Good Morning 😃"

# This job will run at the begining of the pipeline
hello_world:
  stage: .pre
  tags: [ saas-linux-large-amd64 ]
  script: 
    - echo "👋 Hello World 🌍"

# Sast won't scan the `/tests` directory
variables:
  SAST_EXCLUDED_PATHS: /tests

# Include SAST template
include:  # Execute individual project's configuration (if project contains .gitlab-ci.yml)
  - project: '$CI_PROJECT_PATH'
    file: '$CI_CONFIG_PATH'
    ref: '$CI_COMMIT_REF_NAME' # Must be defined or MR pipelines always use the use default branch
  - template: Jobs/SAST.latest.gitlab-ci.yml
  #- template: Security/SAST.gitlab-ci.yml

# Override the SAST Job
semgrep-sast:
  tags: [ saas-linux-large-amd64 ]
```

We are going to "link" this pipeline to a compliance framework. For all projects applying the compliance framework, this pipeline will be triggered automatically.

## 2- Create a Compliance framework

- Go to the highest level group.
- Select **Settings / General** with the left side menu
- Expand the **Compliance frameworks** section
- Click on the <kbd>Add framework</kbd> button 
- And fill the form: (call the framework **sast compliance framework**)

![alt compliance-pipeline](imgs/compliance-01.png)

!!! info
    The compliance pipeline configuration value is composed like this: `<yaml file name of the compliance pipeline>@<path to the project><project name>`, like this `.compliance-ci.yml@tanuki-workshops/secure-demos/sast-compliance-pipeline`.

- Then click on the <kbd>Add framework</kbd> button

## 3- Create a new project and associate a compliance framwork

- Import the project: `templates/demo-project-for-compliance` to create a new project and name it **"sast-compliance-demo"**.
- Apply the compliance framework to the project
  - Select **Settings / General** with the left side menu
  - Expand the **Compliance frameworks** section
  - Choose **sast compliance framework** in the list
  - Click on the <kbd>Save changes</kbd> button 

![alt compliance-pipeline](imgs/compliance-02.png)

- Return to the list of the projects, you can noticed that a new label appeaes at the side of the name of the project

![alt compliance-pipeline](imgs/compliance-03.png)

## 4- Add a vulnerability and a project pipeline

- Move the `tests/main.ts` file to the root of the project
- Move the `tests/.gitlab-ci.yml` file to the root of the project
- Commit on the `main` branch

![alt compliance-pipeline](imgs/compliance-04.png)

- The pipeline is triggered

![alt compliance-pipeline](imgs/compliance-05.png)

> You can notice that the 3 jobs of the compliance pipeline has been executed, even if they are not defined in the project pipeline.

- A vulnerability has been detected

![alt compliance-pipeline](imgs/compliance-06.png)

That's it for this tutorial.
