# 🪲 Container & Dependency scanning
> Detect log4j vulnerabilities


!!! info
    - This tutorial is inspired by this blog post about **the Log4Shell exploit**: [How to use GitLab security features to detect log4j vulnerabilities](https://about.gitlab.com/blog/2021/12/15/use-gitlab-to-detect-vulnerabilities/)
    - Documentation: [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
    - Documentation: [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)


## 1- Create a new project and add a pipeline

- Import the project: `templates/log4j-demo-project` to create a new project and name it **"log4j-demo"**.
  - 👋 **It's a Java project with Log4J dependencies**
- Copy the `tmp/.gitlab-ci.yml` file to the root of the project:

![alt log4shell-exploit](imgs/log4shell-01.png)

- The Java application is "dockerized"
  - The pipeline will build the image of the application (and publish it to the GitLab Container Registry)
  - Then, 2 secure jobs (dependency scanning and container scanning) will be executed
- Commit the changes on the `main` branch
- Wait until the end of the pipeline

![alt log4shell-exploit](imgs/log4shell-02.png)

This is the source code of the pipeline:

```yaml
stages:
  - build
  - test

include:
  - template: Jobs/Dependency-Scanning.latest.gitlab-ci.yml
  - template: Security/Container-Scanning.latest.gitlab-ci.yml

variables:
    CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN: "false" # scan everything even if it's not current OS related
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

container_scanning:
  tags: [ saas-linux-large-amd64 ]
  variables:
    GIT_STRATEGY: fetch # if you want to use the auto remediation

build-project-docker-image:
  stage: build
  tags: [ saas-linux-large-amd64 ]
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_MERGE_REQUEST_IID
  variables:
    JSON_CONFIG: |
      {
        "auths": {
          "$CI_REGISTRY": {
            "username": "$CI_REGISTRY_USER",
            "password": "$CI_REGISTRY_PASSWORD"
          }
        }
      }
  script: |
    echo "${JSON_CONFIG}" > /kaniko/.docker/config.json
    /kaniko/executor \
      --context $CI_PROJECT_DIR \
      --dockerfile $CI_PROJECT_DIR/Dockerfile \
      --cache=true \
      --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
```

## 2- Once the pipline finished

- When the pipeline is finished, you can see a new **Security** tab in the pipeline page:

![alt log4shell-exploit](imgs/log4shell-03.png)

- With the list of all the detected vulnerabilities:

![alt log4shell-exploit](imgs/log4shell-04.png)

- You can filter the vulnerabilities by **Severity** and **Tool** (scanner). It's very important, because it's helpful for the triage by the team.

![alt log4shell-exploit](imgs/log4shell-05.png)

> - 👋 handling vulnerabilities is not magic: the triage task is mandatory, and you have to insert it in the developement workflow.

- If you click on a vulnerability, you get the vulnerability details page:

![alt log4shell-exploit](imgs/log4shell-06.png)

> - From this page, you get the proposal of solution
> - You can dismiss the vulnerability
> - You can directly create an issue

- Exit of the vulnerability page
- From the left side menu, select **Security & Compliance / Vulnerability report**:

![alt log4shell-exploit](imgs/log4shell-07.png)

## 3- Vulnerabilities triaging
> You can adapt the workflow - this is only a demo

### Create a first issue (Container vulnerability)

- Select the vulnerability about the **openjdk8** (detected by the Container Scanning)
- You get the vulnerability page

![alt log4shell-exploit](imgs/log4shell-08.png)

- You can see that you can directly fix it from the <kbd>Resolve with merge request</kbd> button
- But, go to the bottom of the page and click on the <kbd>Create an issue</kbd> button

![alt log4shell-exploit](imgs/log4shell-09.png)

- And create the issue

### Create a second issue (Dependency vulnerability)

- Go back to the **Vulnerability report**
- Filter on **Tool**: select only **Dependency Scanning**

![alt log4shell-exploit](imgs/log4shell-10.png)

- Select the vulnerability about `Improper Neutralization of Special Elements used in an OS Command`
- Create an issue from it
- Go back again to the **Vulnerability report**
- Filter on **Activity**: select only **Has issue**:

![alt log4shell-exploit](imgs/log4shell-11.png)

- You get the 2 vulnerabilities for wich we created an issue:

![alt log4shell-exploit](imgs/log4shell-12.png)

- Select the 2 vulnerabilities (with the check-box) and set their status to **Confirmed**

![alt log4shell-exploit](imgs/log4shell-13.png)

- Then click on the <kbd>Change status</kbd> button to apply the choice

![alt log4shell-exploit](imgs/log4shell-14.png)

Now, it's time to fix the vulnerabilities

## 4- Fix the vulnerabilities

- Go back to the list of the issues
- Choose the one about **openjdk8**
- Create a merge request from this issue
- From the merge request, edit the code with the Web IDE
- In `Dockerfile`, Replace `FROM openjdk:8u181-jdk-alpine` by `FROM openjdk:latest`

![alt log4shell-exploit](imgs/log4shell-15.png)

- We will use the same merge request to fix the other vulnerability
- If you read the vulnerability paage, the solution was to update the `org.springframework.boot` to `2.6.6`
- In `build.gradle`, change all the version number `2.6.1` by `2.6.6`

![alt log4shell-exploit](imgs/log4shell-16.png)

- Commit your changes
- A Merge Request pipeline is started

![alt log4shell-exploit](imgs/log4shell-17.png)

- Once the Merge Request pipeline is finished, you can check the **fixed** vulnerabilties:

![alt log4shell-exploit](imgs/log4shell-18.png)

- Click on the <kbd>Merge</kbd> button and wait for the end of the pipeline of the `main` branch:

![alt log4shell-exploit](imgs/log4shell-19.png)

- Once the pipeline is finished, go back to the **vulnerability report**:

![alt log4shell-exploit](imgs/log4shell-20.png)

- You can notice that a lot of vulnerabilities has been remediated (the little blue label == "no longer detected")
- Then you need to declare these vulnerabilities as "fixed" (set the status to **Resolve**):

![alt log4shell-exploit](imgs/log4shell-21.png)

![alt log4shell-exploit](imgs/log4shell-22.png)

That's it for this tutorial.

















