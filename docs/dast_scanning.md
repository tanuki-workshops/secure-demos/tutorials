# 🍜 DAST Scanning

!!! info
    - Documentation: [DAST Scanning](https://docs.gitlab.com/ee/user/application_security/dast/)

- Import the project: `templates/dast-api-demo` to create a new project and name it **"dast-api-demo"**

## About this project

This project is a Node.js project. It uses the [Fastify](https://www.fastify.io/) framework to serve an API. The application is dockerized, then you can build the Docker image of the application and use the container as a service to test is with the DAST scanner.

The API is simple:
```javascript
fastify.get('/api/hello', async function (req, res) {
  const getRandomNumberBetween = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  const randomNumber = getRandomNumberBetween(0,50);

  res
    .header('Content-Type', 'application/json; charset=utf-8')
    .send({message: "Hello World", result: randomNumber});
});
```

Have a look to the `.gitlab-ci.yml` file:
```yaml
stages:
  - build

build-project-docker-image:
  stage: build
  tags: [saas-linux-large-amd64]
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_MERGE_REQUEST_IID
  variables:
    JSON_CONFIG: |
      {
        "auths": {
          "$CI_REGISTRY": {
            "username": "$CI_REGISTRY_USER",
            "password": "$CI_REGISTRY_PASSWORD"
          }
        }
      }
  script: |
    echo "${JSON_CONFIG}" > /kaniko/.docker/config.json
    /kaniko/executor \
      --context $CI_PROJECT_DIR \
      --dockerfile $CI_PROJECT_DIR/Dockerfile \
      --cache=true \
      --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
```

This CI job (`build-project-docker-image`) build the docker image of the application and publish it to the [GitLab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/) with [Kaniko](https://github.com/GoogleContainerTools/kaniko).

## Add the DAST scanning job and createa merge request

You will update the `.gitlab-ci.yml` file.

Add a new stage `dast` to the `stages` section:
```yaml
stages:
  - build
  - dast
```

Then, add the DAST template:
```yaml
stages:
  - build
  - dast

include:
  - template: Security/DAST.gitlab-ci.yml
```

Then, after the `` job, add a new job named `dast`:
```yaml
dast:
  tags: [saas-linux-large-amd64] # this is not mandatory
  variables:
    DAST_WEBSITE: http://webapp:8080/api/hello
    DAST_FULL_SCAN_ENABLED: "true"
    DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST crawler
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_MERGE_REQUEST_IID
  services: # use services to link your app container to the job
    - name: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
      alias: webapp
```

We will use the `services` feature of GitLab CI: 
- The runner will run a container from the application docker image of the application (the `alias` is the domain name of the application):
  ```yaml
  services:
    - name: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
      alias: webapp
  ```
- Then the scanner will be able to call the api with this domain: `http://webapp:8080`
  ```yaml
  variables:
    DAST_WEBSITE: http://webapp:8080/api/hello
  ```

### Commit and create a merge request from your updates

Commit your code:
![alt dast-01](imgs/dast-01.png)

And start a merge request:
![alt dast-02](imgs/dast-02.png)

## DAST Scanning

A new pipeline is triggered:
![alt dast-03](imgs/dast-03.png)

The first job is building the image of the application
![alt dast-04](imgs/dast-04.png)

Once the image built, the second job starts the application container and runs the DAST scan:
![alt dast-05](imgs/dast-05.png)

On the left side menu, if you select **Packages and registries**, then **Container Registry**, you can check that the image is available:
![alt dast-06](imgs/dast-06.png)

Wait until the end of the SAST scan job:
![alt dast-07](imgs/dast-07.png)

Go back to the merge request; the DAST scanner has detected vulnerabilities:
![alt dast-08](imgs/dast-08.png)

Click on `Missing X-Content-Type-Options: nosniff` line to see the detail of the vulnerability and the solution proposal:
![alt dast-09](imgs/dast-09.png)

![alt dast-10](imgs/dast-10.png)

## Fix the vulnerabilty

To fix the vulnerability, update the source code of `index.js` by adding this line to the response command:

```javascript
.header('X-Content-Type-Options', 'nosniff')
```
![alt dast-11](imgs/dast-11.png)

Then commit the change (on the same merge request) and wait until the end of the pipeline:
![alt dast-12](imgs/dast-12.png)

Now, you can check that the vulnerability has disappeared:
![alt dast-13](imgs/dast-13.png)

That's it for this tutorial.

