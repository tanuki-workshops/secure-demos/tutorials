# 🔗 Dependency Scanning

!!! info
    - Documentation: [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)

- Import the project: `templates/dependency-scanning` to create a new project and name it **"dependency-scanning-demo"**
- The `package.json` file is already generated (with old dependencies)
- The `yarn.lock` file is already generated

## 1- Add the dependency scanning template to the `.gitlab-ci.yml` file
> Open the project with the WEB IDE or GitPod (or clone the project and open it with your own IDE)

Add the below section to your pipeline:

```yaml
# 🤚 the path of the template is subject to change
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
```

And override the dependency scanning job to keep the MR pipeline:

```yaml
# override the dependency scanning job
gemnasium-dependency_scanning:
  tags: [ saas-linux-large-amd64 ]
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_MERGE_REQUEST_IID
```
> - 🤚 the rules are not anymore mandatory
> - tags are only if you use GitLab.com

> **The `test` stage in the pipeline is mandatory**

### Before

```yaml
stages:
  - build
  - test

dummy-job:
  stage: test
  script:
    - echo "👋 hello world 🌍"
```

### After

```yaml
stages:
  - build
  - test

# 🤚 the path of the template is subject to change
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

# override the dependency scanning job
gemnasium-dependency_scanning:
  tags: [ saas-linux-large-amd64 ]
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_MERGE_REQUEST_IID

dummy-job:
  tags: [ saas-linux-large-amd64 ]
  stage: test
  script:
    - echo "👋 hello world 🌍"
```

## 2- Commit the changes

- Commit the changes on the `main` branch
- Observe the CI pipeline
- Show the pipeline security report (last **Security** tab in the pipeline details page)
- Click on a vulnerability to show the details

### Vulnerability report

- Go to the vulnerability report:
  - From the left side menu, choose **Security and Compliance**, then **Vulnerability Report**
- Select the 3 **minor** vulnerabilities, and change the status to **"Dismiss"** (will not fix or false positive)
- Select the **high** vulnerability, anc change the status to "Confirmed"

### Fix the vulnerability

- Click on the vulnerability to read the details of it

At the bottom of the issue, you should read:

```text
Solution Upgrade to version 4.8.1 or above. 

 
      Evidence
     Vulnerable Package fastify:2.14.1
```
- Click on <kbd>Create issue</kbd>
- Assign the issue to yourself
- Then, create the issue

#### Create a Merge Request

- Create a Merge Request from the issue
- Assign yourself to the MR
- Click on <kbd>Create merge request</kbd>
- Open the MR from GitPod (or `git pull and checkout the feature branch` and open it with your own IDE)
  - You need to open the MR in a real development environment, because you need to execute **yarn** to update the dependencies
- Change the content of `package.json`:

```json
{
  "dependencies": {
    "fastify": "4.8.1",
    "fastify-static": "4.4.1"
  }
}
```

- Run the `yarn` command to update the dependencies (and update the `yarn.lock` file)
- Commit your changes => a merge request pipeline will be triggered
- Wait for the end of the pipeline

At the end of the pipeline, in the MR widget, you should get this message "Security scanning detected no new vulnerabilities."
And a report like the one below, explaining that the high vulnerability are fixed, and the others are dismissed:

![alt mr-report](imgs/mr-report.png)

In the MR, if you click on the <kbd>Full report</kbd> button, you'll get an empty pipeline report (because everything is fixed)

You can mark the MR "as ready" (click on <kbd>Mark as ready</kbd>) and merge the MR.

## 3- Add a new vulnerability

- Create a new issue: "add axios dependency"
- Create a MR from this issue
- Open the MR from GitPod (or `git pull and checkout the feature branch` and open it with your own IDE)
- Add a new dependency to `package.json`, add axios:

```json
{
  "dependencies": {
    "fastify": "4.8.1",
    "fastify-static": "4.4.1",
    "axios": "0.21.0"
  }
}
```
- Run the `yarn` command to update the dependencies (and update the `yarn.lock` file)
- Commit your changes

At the end og the pipeline, in the MR widget, you should get this message "Security scanning detected 2 potential vulnerabilities 0 Critical 1 High and 1 Other."
And a report like the one below:

![alt mr-report](imgs/mr-report-vulnerability.png)

In the MR, if you click on the <kbd>Full report</kbd> button, you'll get the pipeline report with the two vulnerabilities:

![alt mr-report](imgs/mr-pipeline-report.png)

That's it for this tutorial.
