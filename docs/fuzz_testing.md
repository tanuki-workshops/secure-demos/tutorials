# 🧁 Fuzz Testing

!!! info
    - Documentation: [Coverage-guided fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)
    - Project: [gitlab-cov-fuzz, the official gitlab CLI that helps running fuzz tests inside gitlab CI/CD](https://gitlab.com/gitlab-org/security-products/analyzers/gitlab-cov-fuzz)
    - Projects: [Various fuzzers that are compatible with GitLab fuzzing](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers)
    - Documentation: [Web API Fuzz Testing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/)

- Import the project: `templates/fuzz-testing` to create a new project and name it **"fuzz-testing-demo"**

## About this project

This project is a Node.js project with some JavaScript functions in the `my-tools.js` file:

```javascript
const fs = require('fs')

function sayHello(name) {
  if(name.includes("z")) {
    throw new Error("😡 error name: " + name)
  } else {
    return "😀 hello " + name
  }
}

function readmeContent(name) {

  let fileName = name => {
    if(name.includes("w")) {
      return "./README.txt"
    } else {
      return "./README.md"
    }
  }

  const data = fs.readFileSync(fileName(name), 'utf8')

  return data

}

module.exports = {
  sayHello, readmeContent
}
```

In the next sections, we will 

- write two JavaScript **fuzz targets** (each fuzz target will call a function of the `my-tools.js` library)
- design a GitLab CI pipeline running the **gitlab-cov-fuzz** CLI to execute the JavaScript fuzzer on the two **fuzz targets**

## Fuzz targets

The fuzz target call a function (of `my-tools.js`) with a random buffer as parameter.

Add two files at the root of the project:
- `fuzz-sayhello.js`
- `fuzz-readme.js`

with the following content:

**`fuzz-sayhello.js`**
```javascript
let tools = require('./my-tools')

function fuzz(buf) {
  const text = buf.toString()
  tools.sayHello(text)
}

module.exports = {
  fuzz
}
```

**`fuzz-readme.js`**
```javascript
let tools = require('./my-tools')

function fuzz(buf) {
  const text = buf.toString()
  tools.readmeContent(text)
}

module.exports = {
  fuzz
}
```

## Call the JavaScript Fuzzer from GitLab CI

Add a `.gitlab-ci.yml` file at the root of the project. To enable coverage-guided fuzz testing, you need a `fuzz` stage and you have to include the `Coverage-Fuzzing.gitlab-ci.yml` template:

```yaml
image: node:18

stages:
  - fuzz

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml
```

Then, add two jobs to the pipeline. Each job will run the **[jsfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/jsfuzz)** engine with the **gitlab-cov-fuzz CLI** using the fuzz targets as parameter:

```yaml
readme_fuzz_target:
  extends: .fuzz_base
  tags: [saas-linux-large-amd64] # this is not mandatory
  variables:
    COVFUZZ_ADDITIONAL_ARGS: '--fuzzTime=60'
  script:
    - npm config set @gitlab-org:registry https://gitlab.com/api/v4/packages/npm/ && npm i -g @gitlab-org/jsfuzz
    - ./gitlab-cov-fuzz run --engine jsfuzz -- fuzz-readme.js

hello_fuzzing_target:
  extends: .fuzz_base
  tags: [saas-linux-large-amd64] # this is not mandatory
  variables:
    COVFUZZ_ADDITIONAL_ARGS: '--fuzzTime=60'
  script:
    - npm config set @gitlab-org:registry https://gitlab.com/api/v4/packages/npm/ && npm i -g @gitlab-org/jsfuzz
    - ./gitlab-cov-fuzz run --engine jsfuzz -- fuzz-sayhello.js

```

- **Jsfuzz** will call the fuzz target in an infinite loop with random data.
- The function must catch and ignore any expected exceptions that arise when passing invalid input to the tested package.
- The fuzz target must call the function of the library with the passed buffer or a transformation on the test buffer.
- **Jsfuzz** will report any unhandled exceptions as crashes.

## Run the pipeline

Now, you can commit your files and create a merge request. This will trigger a new pipeline.

![alt fuzz-01](imgs/fuzz-01.png)

![alt fuzz-02](imgs/fuzz-02.png)

Once the pipeline is finished, you can read the security pipeline report:

![alt fuzz-03](imgs/fuzz-03.png)

You will retrieve the report inside the merge request widget:

![alt fuzz-04](imgs/fuzz-04.png)

And you can read the detail of the detected vulnerabilities when you click on the related row:

![alt fuzz-05](imgs/fuzz-05.png)

![alt fuzz-06](imgs/fuzz-06.png)

## Fix the vulnerabilities

You need to catch the errors in the functions of the `my-tools.js` library. So, update `my-tools.js` like below:

```javascript
const fs = require('fs')

function sayHello(name) {
  if(name.includes("z")) {
    //throw new Error("😡 error name: " + name)
    console.log("😡 error name: " + name)
  } else {
    return "😀 hello " + name
  }
}

function readmeContent(name) {

  let fileName = name => {
    if(name.includes("w")) {
      return "./README.txt"
    } else {
      return "./README.md"
    }
  }

  //const data = fs.readFileSync(fileName(name), 'utf8')
  try {
    const data = fs.readFileSync(fileName(name), 'utf8')
    return data
  } catch (err) {
    console.error(err.message)
    return ""
  }
  

}

module.exports = {
  sayHello, readmeContent
}
```

Then, commit your changes (on the same branch). This will trigger again a pipeline:

![alt fuzz-07](imgs/fuzz-07.png)

And you can notice that the vulnerabilities disappeared:

![alt fuzz-08](imgs/fuzz-08.png)

You can now, merge your changes.

That's it for this tutorial.



