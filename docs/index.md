# 🍄 Secure Tutorials
  
## Tutorials

- [🐞 SAST scanning](sast/)
- [🔗 Dependency Scanning](dependency_scanning/)
- [🥇 License Compliance](license_compliance/)
- [🛠️ Compliance pipeline](compliance_pipeline/)
- [🔬 Scan result policy](scan_result_policy/)
- [🪲 Container & Dependency scanning](container_dependency_scanning/) *detect log4j vulnerabilities*
- [🍜 DAST Scanning](dast_scanning/)
- [🧁 Fuzz Testing](fuzz_testing/)
