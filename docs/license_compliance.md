# 🥇 License Compliance

!!! info
    - Documentation: [License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/)
    - For this demonstration we are going to use a Python project.

- Import the project: `templates/license-compliance` to create a new project and name it **"license-compliance-demo"**

## 1- Add the Python dependencies and the CI pipeline
> Open the project with the WEB IDE or GitPod (or clone the project and open it with your own IDE)

### Dependencies

Copy the `assets/requirements.txt` file to the root of the project

We added the **keras** library to our project (this is the content of `requirements.txt`):
```
keras==2.0.0
```

### CI pipeline

Copy the `assets/.gitlab-ci.yml` file to the root of the project:

```yaml
stages:
  - build
  - test

include:
  - template: Security/License-Scanning.gitlab-ci.yml

dummy-job:
  stage: test
  tags: [ saas-linux-large-amd64 ]
  script:
    - echo "pipeline must contain at least 1 job definition ..."

license_scanning:
  tags: [ saas-linux-large-amd64 ]
```

> - The `test` stage is mandatory
> - You only need to include the license scanning template
> - You can override the `license_scanning`
>   - Remark: `saas-linux-large-amd64` is a tag to use tne new [SaaS Linux Runners](https://about.gitlab.com/blog/2022/09/22/new-machine-types-for-gitlab-saas-runners/) (it's optional)

## 2- Commit the changes

- Commit the changes on the `main` branch
- Observe the CI pipeline
- Show the pipeline licenses report (last **Licenses** tab in the pipeline details page)

![alt lic-report](imgs/lic-scann-pipeline-1.png)

![alt lic-report](imgs/lic-scann-pipeline-2.png)

And you can retrieve the list of licenses in the **License Compliance** report: from the left side menu, choose **Security and Compliance**, then **License Compliance**

![alt lic-report](imgs/lic-scann-report-1.png)

## 3- Add Policies

Click on the **Policies** tab, 

![alt lic-report](imgs/lic-scann-add-policies-1.png)

Then use the <kbd>Add license policy</kbd> button to add the following policies:

- MIT License / `Allow`
- Apache License 2.0 / `Deny`
- ISC License  / `Deny`

![alt lic-report](imgs/lic-scann-add-policies-2.png)

![alt lic-report](imgs/lic-scann-add-policies-3.png)

Then if you go back to the previous tab, you can notice that the list of the licenses is updated and a Policy violation is detected:

![alt lic-report](imgs/lic-scann-add-policies-4.png)

## 4- Add an approval rules

- Go back to the **Policies** tab 
- Click on the <kbd>Update approvals</kbd> button
- Add 2 approvers (or less or a group)

![alt lic-report](imgs/lic-scann-add-policies-5.png)

> You can notice now that the **"License Approvals are active"**:

![alt lic-report](imgs/lic-scann-add-policies-6.png)

## 5- Add a new dependency (with a Merge Request)

- Edit the project with the Web IDE
- Add the content of `assets/requirements.2.txt` to `./requirements.txt`

The content of `./requirements.txt` should be like this:

```
keras==2.0.0
dnspython==2.1.0
```

Then commit on a new branch to create a Merge Request

![alt lic-report](imgs/lic-scann-mr-1.png)

And wait until the end of the triggered MR pipeline. Once the pipeline finished, you can show that there is a violation of the compliance and that you need an approval:

![alt lic-report](imgs/lic-scann-mr-2.png)

If the MR is approved (the policy is bypassed by the approver), a new violation will be displayed in the License Compliance report:

![alt lic-report](imgs/lic-scann-mr-3.png)

That's it for this tutorial.





