# 🐞 SAST scanning

!!! info
    - Documentation: [SAST](https://docs.gitlab.com/ee/user/application_security/sast/)
    - For this demonstration we are going to use a Java project.

## Create a simple project with a first SAST Scan

### Initialize & activate the SAST scan

- Create a new GitLab blank project (with a `README.md` file)
- Add a `.gitlab-ci.yml` file to the root of the repository

Update the `.gitlab-ci.yml` file with the following content:

```yaml
stages:
  - build
  - test

include:
  - template: Security/SAST.gitlab-ci.yml
  #- template: Jobs/SAST.gitlab-ci.yml

building:
  stage: build
  script:
    - echo "building"
```

To activate the SAST scan in a GitLab pipeline, you only need to add a `test` stage and include the SAST template:

```yaml
include:
  - template: Jobs/SAST.gitlab-ci.yml
```

> See the GitLab documentation: [https://docs.gitlab.com/ee/user/application_security/sast/#configure-sast-in-your-cicd-yaml](https://docs.gitlab.com/ee/user/application_security/sast/#configure-sast-in-your-cicd-yaml)

### "Create" a vulnerability

Create a new Java file: `JaxRsEndpoint.java` with the following content:

```java
// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;
import javax.ws.rs.Path;
import org.apache.commons.text.StringEscapeUtils;

@Path("/test")
public class JaxRsEndpoint {

    public String randomFunc(String s) {
        return s;
    }

    @Path("/hello0")
    public String hello0(String user) {
        return "Hello " + user; // BAD
    }

     @Path("/hello1")
    public String hello1(String user) {
        String tainted = randomFunc(user);
        return "Hello " + tainted;  // BAD
    }

    @Path("/hello2")
    public String hello2(String user) {
        String sanitized = StringEscapeUtils.unescapeJava(user);
        return "Hello " + sanitized; // OK
    }
} 
```

And commit the changes on the `main` branch.

## First SAST scan

The commit will trigger a new pipeline with a `semgrep-sast` job (belonging to the `test` stage).

When the pipeline is finished, if you look at the pipeline details page, you can see a new **Security** tab. If you click on this tab, you'll obtain the list of the **detected** vulnerabilities (2 row with this label: "Improper Input Validation"). You can click on the label of a vulnerability row to get the details of this vulnerability.

On the lateral left side menu, if you choose the **Security and Compliance** menu and then, the **Vulnerability Report** option, you'll obtain a more detailed report with the two discovered vulnerabilities.

> 🤚 The usage of the **Vulnerability Report** will be the subject of another tutorial.

### Fix the vulnerabilities

To quickly fix the vulnerabilties of `JaxRsEndpoint.java`, remove the body content of the `JaxRsEndpoint` class:

```java
// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;
import javax.ws.rs.Path;
import org.apache.commons.text.StringEscapeUtils;

@Path("/test")
public class JaxRsEndpoint {

}
```

Then, commit the changes on the `main` branch. The commit will trigger a new pipeline with a `semgrep-sast` job. When the pipeline is finished, if you look at the pipeline details page, you can see the **Security** tab is now empty.

If you return to the **Vulnerability Report**, the two vulnerabilities remain, but these bulnerabilities are marked as **remediated**.

> - The vulnerability are no longer detected. Verify the vulnerabilities have been fixed or removed before changing its status.
> - 🤚 The usage of the **Vulnerability Report** will be the subject of another tutorial.

## Add a new vulnerability trough a Merge Request

Create a new merge request and add a new Java file to the repository: `PseudoRandom.java` with the following content:

```java
// License: LGPL-3.0 License (c) find-sec-bugs
package random;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.util.Random;

public class PseudoRandom {
    static String randomVal =  Long.toHexString(new Random().nextLong());

    public static String generateSecretToken() {
        Random r = new Random();
        return Long.toHexString(r.nextLong());
    }

    public static String count() {
        return RandomStringUtils.random(10);
    }

    public static long getRandomVal() {
        return RandomUtils.nextLong();
    }
}
```

Then, commit the changes on the feature branch related to the MR. This will trigger a merge request pipeline. Once the pipeline is finished, **five new vulnerabilities** are displayed inside the **Security scanning widget**. If you click on a vulnerability row of the widget, you obtain the details of the related vulnerability.

### Fix the vulnerabilities

To quickly fix the vulnerabilties of `PseudoRandom.java`, remove the body content of the `PseudoRandom` class:

```java
// License: LGPL-3.0 License (c) find-sec-bugs
package random;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.util.Random;

public class PseudoRandom {

}
```

Then, commit the changes on the same feature branch. This will trigger again a merge request pipeline. Once the pipeline is finished, The **Security scanning widget** displays that SAST detected no new vulnerabilities.

