# 🔬 Scan Result Policy

!!! info
    - Documentation: [Scan result policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)


## 1- Create a new project

- Import the project: `templates/demo-project-for-compliance` to create a new project and name it **"sast-scan-result-policy"**.

## 2- Add a Scan Result Policy

- In the left side menu, go to **Security & Compliance / Policies**

![alt scan-result-policy](imgs/policy-01.png)

- Click on the <kbd>New policy</kbd> button
- Select **Scan result policy**

![alt scan-result-policy](imgs/policy-02.png)

### Define the scan result policy

- Fill the name and the description of the policy

![alt scan-result-policy](imgs/policy-03.png)

- Define a rule that will trigger the policy and an action

![alt scan-result-policy](imgs/policy-04.png)

- Click on the <kbd>Configure with a merge request</kbd> button
  - When you click on the button, a new project is created to store the policies linked to the project
  - And, at the same time a Merge request to define the policy is initialized

![alt scan-result-policy](imgs/policy-05.png)

- Click on the <kbd>Merge</kbd> button
- If you return to the list of the projects, you can see the new project

![alt scan-result-policy](imgs/policy-06.png)

## 3- Add a pipeline and some vulnerabilities to the sast-scan-result-policy project

- Go back to the project (`sast-scan-result-policy`)
- If you select again **Security & Compliance / Policies**, you can check the list of the policies

![alt scan-result-policy](imgs/policy-07.png)

### Create a pipeline

- With the Web IDE, add a `.gitlab-ci.yml` file at the root of the project with the following content:

```yaml
stages:
  - test
  - greetings

# Sast won't scan the `/tests` directory
variables:
  SAST_EXCLUDED_PATHS: /tests

# Include SAST template
include:
  - template: Jobs/SAST.latest.gitlab-ci.yml

good_morning:
  stage: greetings
  tags: [ saas-linux-large-amd64 ]
  script: 
    - echo "🥐 Good Morning 😃"

# Override the SAST Job
semgrep-sast:
  tags: [ saas-linux-large-amd64 ]
```
> - you can use the content of the `tests/.gitlab-ci.policy.yml` file
> - The overriding of the `semgrep-sast` job is optional
> - Using `tags: [ saas-linux-large-amd64 ]` is optional (it works only on GitLab.com)


### Add some vulnerabilities

- Move the `tests/main.ts` file to the root of the project
- Move the `tests/index.js` file to the root of the project
- Commit on a new branch, with the option `Start a new merge request`

![alt scan-result-policy](imgs/policy-08.png)

- On the next screen (the Merge request one) click on the <kbd>Create merge request</kbd> button

![alt scan-result-policy](imgs/policy-09.png)

- A pipeline will start:

![alt scan-result-policy](imgs/policy-10.png)

- You can check the list of the vulnerabilities with the security pipeline report:

![alt scan-result-policy](imgs/policy-11.png)

- And if you return to the MR screen, you can notice that the MR is blocked, waiting for approvals:

![alt scan-result-policy](imgs/policy-12.png)

That's it for this tutorial.








